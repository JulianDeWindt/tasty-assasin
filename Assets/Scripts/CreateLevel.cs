﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateLevel : MonoBehaviour
{
    public GameObject groundBackground;
    public GameObject groundGrass01;
    public Sprite groundGrass02;
    public Sprite groundGrass03;
    public Sprite groundGrass04;
    public GameObject ground01;
    public Sprite ground02;
    public Sprite ground03;
    public Sprite ground04;

    public GameObject mais;
    public GameObject bulletPickup;
    public GameObject strawberry;
    public GameObject chicken;
    public GameObject dog;
    public GameObject milestone;
    public GameObject beststone;


    float createdTill = 0;
    float currentHeight = 0;
    int maisChance = 95;
    int platformChance = 95;
    int last = 0; //0: Air, 1: Mais, 2: berry
    bool lastDown = false;
    bool lastCanyon = true;
    bool lastPlatform = false;
    bool lastlastPlatform = false;
    int lastEnemy = 5;

    double characterPos = 0.0;

    // Start is called before the first frame update
    void Start()
    {
        //Maisstart
        for (int i = -15; i <= 0; i++) {
            Vector3 position = new Vector3(i, currentHeight, 0);
            createGrass(position);
            for (float j = 1; j <= currentHeight + 7; j++) {
                position = new Vector3(i, currentHeight - j, 0);
                createGround(position);
            }
            position = new Vector3(i, currentHeight + 1.5f, 0);
            GameObject maisGameObject = GameObject.Instantiate(mais, position, Quaternion.identity);
        }

        for (int i = 1; i <= 3; i++) {
            Vector3 pos = new Vector3(i, currentHeight, 0);
            createGrass(pos);
            for (float j = 1; j <= currentHeight + 7; j++) {
                pos = new Vector3(i, currentHeight - j, 0);
                createGround(pos);
            }
        }
        createdTill = 4;
    }

    // Update is called once per frame
    void Update() {
        characterPos = Gamemanager.gamemanager.characterPos;

        if (createdTill <= characterPos + 32) {
            createColumn();
        }

    }
    void createColumn() {
        bool canSpawnEnemy = true;

        if (lastEnemy <= 20 - (Gamemanager.gamemanager.characterPos - (Gamemanager.gamemanager.characterPos % 100)) / 100) {
            canSpawnEnemy = false;
            lastEnemy++;
        }

        if (last != 0 || lastCanyon) {
            canSpawnEnemy = false;
        }

        int canyon = Random.Range(0, 100);

        if (canyon > 80 && !lastCanyon && !lastPlatform) {
            lastCanyon = true;
            createdTill++;
            for (int i = 0; i <= currentHeight + 7; i++) {
                Vector3 pos = new Vector3(createdTill, currentHeight - i, 0);
                GameObject.Instantiate(groundBackground, pos, Quaternion.identity);
                pos = new Vector3(createdTill - 1, currentHeight - i, 0);
                GameObject.Instantiate(groundBackground, pos, Quaternion.identity);
            }
            canSpawnEnemy = false;
        } else {
            //Change Height between 0, 3 20% up/down
            int ran = Random.Range(0, 10);
            if (ran <= 1 && !lastDown && !lastPlatform && !lastCanyon) {
                lastDown = false;
                currentHeight++;
                canSpawnEnemy = false;
            } else if (ran >= 8 && !lastPlatform && !lastCanyon) {
                lastDown = true;
                currentHeight--;
                canSpawnEnemy = false;
            } else {
                lastDown = false;
            }

            if (currentHeight < 0)
                currentHeight = 0;

            if (currentHeight > 3) {
                currentHeight = 3;
            }

            Vector3 pos = new Vector3(createdTill, currentHeight, 0);
            createGrass(pos);
            for (float i = 1; i <= currentHeight + 7; i++) {
                pos = new Vector3(createdTill, currentHeight - i, 0);
                createGround(pos);
            }

            ran = Random.Range(0, 100);
            if (ran == 0) {
                Vector3 berryPos = new Vector3(createdTill, currentHeight + 1, 0);
                GameObject berryObject = GameObject.Instantiate(strawberry, berryPos, Quaternion.identity);
                canSpawnEnemy = false;
                last = 2;
            } else if (ran >= maisChance) {
                ran = Random.Range(0, 10);
                if (ran == 4) {
                    Vector3 bulletPos = new Vector3(createdTill, currentHeight + 1, 0);
                    GameObject bullet = GameObject.Instantiate(bulletPickup, bulletPos, Quaternion.identity);
                }
                Vector3 maisPos = new Vector3(createdTill, currentHeight + 1.5f, 0);
                GameObject maisObject = GameObject.Instantiate(mais, maisPos, Quaternion.identity);
                last = 1;
                if (maisChance == 95)
                    maisChance = 20;
                else
                    maisChance += 25;
                canSpawnEnemy = false;
            } else {
                last = 0;
            }

            ran = Random.Range(0, 100);
            if (ran >= platformChance && (!lastlastPlatform || lastPlatform) && !lastCanyon) {
                Vector3 posPlat = new Vector3(createdTill, currentHeight + 3, 0);
                createGrass(posPlat);
                if (platformChance == 95)
                    platformChance = 10;
                else
                    platformChance += 10;
                lastlastPlatform = lastPlatform;
                lastPlatform = true;
            } else {
                platformChance = 95;
                lastlastPlatform = lastPlatform;
                lastPlatform = false;
            }
            lastCanyon = false;
        }
        
        if (canSpawnEnemy) {
            Vector3 enemyPos = new Vector3(createdTill, currentHeight + 1, 0);
            switch (Random.Range(0,2))
            {
                case 0:
                    GameObject.Instantiate(chicken, enemyPos, Quaternion.identity);
                    break;
                case 1:
                    GameObject.Instantiate(dog, enemyPos, Quaternion.identity);
                    break;
                default:
                    break;
            }
            lastEnemy = 0;
        }

        if (Gamemanager.gamemanager.lastDistance == createdTill)
        {
            Vector3 milestonePos = new Vector3(createdTill, currentHeight + 1, 0);
            GameObject.Instantiate(milestone, milestonePos, Quaternion.identity);
        }

        if (Gamemanager.gamemanager.bestDistance == createdTill)
        {
            Vector3 milestonePos = new Vector3(createdTill, currentHeight + 1, 0);
            GameObject.Instantiate(beststone, milestonePos, Quaternion.identity);
        }

        createdTill++;
    }

    private void createGrass(Vector3 pos) {
        int ran = Random.Range(0, 4);
        GameObject groundGrassObject;
        if (ran <= 75) {
            groundGrassObject = GameObject.Instantiate(groundGrass01, pos, Quaternion.identity);
        } else if (ran <= 85) {
            groundGrassObject = GameObject.Instantiate(groundGrass01, pos, Quaternion.identity);
            groundGrassObject.GetComponent<SpriteRenderer>().sprite = groundGrass02;
        } else if (ran <= 95) {
            groundGrassObject = GameObject.Instantiate(groundGrass01, pos, Quaternion.identity);
            groundGrassObject.GetComponent<SpriteRenderer>().sprite = groundGrass03;
        } else {
            groundGrassObject = GameObject.Instantiate(groundGrass01, pos, Quaternion.identity);
            groundGrassObject.GetComponent<SpriteRenderer>().sprite = groundGrass04;
        }
    }
    private void createGround(Vector3 pos) {
        int ran = Random.Range(0, 100);
        GameObject groundObject;
        if (ran <= 75) {
            groundObject = GameObject.Instantiate(ground01, pos, Quaternion.identity);
        } else if (ran <= 85) {
            groundObject = GameObject.Instantiate(ground01, pos, Quaternion.identity);
            groundObject.GetComponent<SpriteRenderer>().sprite = ground02;
        } else if (ran <= 95) {
            groundObject = GameObject.Instantiate(ground01, pos, Quaternion.identity);
            groundObject.GetComponent<SpriteRenderer>().sprite = ground03;
        } else {
            groundObject = GameObject.Instantiate(ground01, pos, Quaternion.identity);
            groundObject.GetComponent<SpriteRenderer>().sprite = ground04;
        }
    }
}
