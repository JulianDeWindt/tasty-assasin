﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenController : EnemyBase
{
    private void FixedUpdate()
    {

        if (!amIDead) {
            Vector2 lineCastPos = myTrans.position - myTrans.right * (myWidth + 0.1f) - myTrans.up * myHeight * .5f;

            RaycastHit2D groundcast = Physics2D.Linecast(lineCastPos, lineCastPos + Vector2.down, enemyMask);
            bool isGrounded = groundcast;
            Debug.DrawLine(lineCastPos, lineCastPos + Vector2.down);

            RaycastHit2D wallcast = Physics2D.Linecast(lineCastPos, lineCastPos + new Vector2(-myTrans.right.x, myTrans.right.y) * .05f, enemyMask);
            bool isBlocked = wallcast;
            Debug.DrawLine(lineCastPos, lineCastPos + new Vector2(-myTrans.right.x, myTrans.right.y) * .05f);

            Vector3 currRot = myTrans.eulerAngles;

            if (!isGrounded || isBlocked)
            {
                currRot.y += 180;
            }
            currRot.z = 0;

            myTrans.eulerAngles = currRot;

            Vector2 myVel = myBody.velocity;
            myVel.x = -myTrans.right.x * speed;
            myBody.velocity = myVel;
        }else
        {

            myBody.rotation = 0;
            myBody.simulated = false;

        }
    }
}
