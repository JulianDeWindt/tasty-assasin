﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public Vector3 dest;
    public Vector3 move;
    public Animator animator;
    public int popDistance;

    Vector2 start;

    // Start is called before the first frame update
    void Start()
    {
        while (dest == null)
        {
        }

        Vector3 pos = transform.position;
        move = dest - pos;
        move = move.normalized;

        start = GetComponent<Rigidbody2D>().position; 
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().MovePosition(transform.position + move * Time.deltaTime * 15);

        Vector2 tets = GetComponent<Rigidbody2D>().position - start;

        if(Mathf.Sqrt(tets.sqrMagnitude) > popDistance)
        {

            animator.SetBool("pop", true);

        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (animator.GetBool("pop")) {
            Destroy(gameObject);
            return;
        }

        string tag = collision.gameObject.tag;
        if (tag == "Berry")
        {
            Destroy(collision.gameObject);
            Gamemanager.gamemanager.killedBerries++;
            Destroy(gameObject);
        }
        else if (tag == "Chicken")
        {
            EnemyBase test = collision.gameObject.GetComponent<EnemyBase>();
            test.amIDead = true;
            collision.gameObject.GetComponent<Animator>().SetBool("disappear", true);
            Destroy(collision.gameObject, collision.gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
            Destroy(gameObject);
        }
        else if (tag == "Dog")
        {
            EnemyBase test = collision.gameObject.GetComponent<EnemyBase>();
            test.amIDead = true;
            collision.gameObject.GetComponent<Animator>().SetBool("disappear", true);
            Destroy(collision.gameObject, collision.gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
            Destroy(gameObject);
        }
        else if (tag == "Ground")
        {
            Destroy(gameObject);
        }
    }
}
