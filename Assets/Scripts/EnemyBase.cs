﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour
{
    public LayerMask enemyMask;
    public float speed = 1.0f;
    public Animator chickenAnim;
    public Animator dogAnim;

    protected Rigidbody2D myBody;
    protected Transform myTrans;
    protected float myWidth;
    protected float myHeight;
    public bool amIDead = false;

    public AudioClip deathSound;

    private void Start()
    {
        myTrans = this.transform;
        myBody = this.GetComponent<Rigidbody2D>();
        myWidth = this.GetComponent<SpriteRenderer>().bounds.extents.x;
        myHeight = this.GetComponent<SpriteRenderer>().bounds.extents.y;
    }

    void Update()
    {
        if (myTrans.position.y < -3)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Rigidbody2D player = collision.gameObject.GetComponent<Rigidbody2D>();
            if (player.velocity.y < 0 && player.position.y > myBody.position.y)
            {

                amIDead = true;
                gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
                gameObject.GetComponent<Animator>().SetBool("disappear", true);
                Destroy(gameObject, gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
                
            }
            else if (Gamemanager.gamemanager.playerHurt == 0.0f)
            {
                Gamemanager.gamemanager.lives--;
                Gamemanager.gamemanager.playerHurt = 1.5f;
            }
        }
    }

    private void OnDestroy()
    {
        SoundManager.instance.PlaySingle(deathSound);
        Gamemanager.gamemanager.killedEnemies++;

    }
}
