﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{

    public CharacterController2D controller;
    public Animator animator;

    public GameObject bullet;

    public float runSpeed = 40f;

    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;

    //Sound
    public AudioClip moveSound1;
    public AudioClip jumpSound;
    public AudioClip shootSound;
    bool soundJump = false;

    // Update is called once per frame
    void Update() {
        //Pause
        if (Input.GetKeyDown(KeyCode.P)) {
            if (Time.timeScale == 0) {
                Time.timeScale = 1;
            } else if (Time.timeScale == 1) {
                Time.timeScale = 0;
            }
        }

        if (Time.timeScale == 0)
            return;

        Gamemanager.gamemanager.characterPos = gameObject.transform.position.x;

        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if(Input.GetButtonDown("Jump")){

            jump = true;
            animator.SetBool("jumping", true);
            if (soundJump == false) {
                soundJump = true;
                SoundManager.instance.PlaySingle(jumpSound);
            }
        }

        if(Input.GetButtonDown("Crouch")) {

            Debug.Log("hier ducken wir");
            crouch = true;

        }else if(!Input.GetButton("Crouch")) {

            crouch = false;

        }

        //Shooting
        if (Input.GetButtonDown("Fire1")) {
            if (Gamemanager.gamemanager.ammo > 0) {
                Vector3 pos = transform.position;

                if (controller.m_FacingRight)
                    pos = new Vector3(pos.x + 0.5f, pos.y, pos.z);
                else
                    pos = new Vector3(pos.x - 0.5f, pos.y, pos.z);
                Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                pz.z = 0;

                if ((pos.x < pz.x && controller.m_FacingRight) || (pos.x > pz.x && !controller.m_FacingRight)) {
                    GameObject bulletOb = GameObject.Instantiate(bullet, pos, Quaternion.identity);
                    bulletOb.GetComponent<BulletController>().dest = pz;
                    Gamemanager.gamemanager.ammo--;
                    SoundManager.instance.PlaySingle(shootSound);
                }
            }
        }

        if (gameObject.transform.position.y < -3) {
            Gamemanager.gamemanager.Die();
        }
    }

    void FixedUpdate() {
        if (gameObject.transform.position.x < 0 && horizontalMove < 0)
            horizontalMove = 0;

        if (!soundJump &&  horizontalMove != 0 && !SoundManager.instance.efxSource.isPlaying) {
            SoundManager.instance.PlaySingle(moveSound1);
        }
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);

        jump = false;

    }

    public void onCrouching (bool isCrouching)
    {

        animator.SetBool("crouch", isCrouching);

    }

    public void onLanding() {

        animator.SetBool("jumping", false);
        soundJump = false;

    }
}
