﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public Text highscoreText;
    public Text lastscoreText;
    public Canvas back;
    public Canvas start;

    // Start is called before the first frame update
    void Start()
    {
        Gamemanager.gamemanager.highscore = PlayerPrefs.GetInt("highscore", 0);
        Gamemanager.gamemanager.bestDistance = PlayerPrefs.GetInt("bestDistance", 0);
        if (Gamemanager.gamemanager.repeat) {

            start.gameObject.SetActive(false);
            back.gameObject.SetActive(true);

            Debug.Log("wait");

            StartCoroutine(Example());

        }else
        {

            back.gameObject.SetActive(false);

        }

    }

    IEnumerator Example()
    {
        yield return new WaitForSeconds(5);
        start.gameObject.SetActive(true);
        back.gameObject.SetActive(false);
        Gamemanager.gamemanager.repeat = false;
    }

    // Update is called once per frame
    void Update() {
        highscoreText.text = Gamemanager.gamemanager.highscore.ToString();
        lastscoreText.text = Gamemanager.gamemanager.score.ToString();
    }
}
