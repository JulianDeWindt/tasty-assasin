﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Berry : MonoBehaviour
{
    public int index;
    Image img;
    public Image background;

    // Start is called before the first frame update
    void Start()
    {
        img = this.GetComponent<Image>();
        background.enabled = false;
        img.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Gamemanager.gamemanager.lives < index)
        {
            img.enabled = false;
            background.enabled = true;
        } else
        {
            img.enabled = true;
            background.enabled = false;
        }
    }
}
