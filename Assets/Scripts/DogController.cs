﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogController : EnemyBase
{
    bool jumping = false;
    public float jumpforce = 200;
    public Animator animator;

    private void FixedUpdate()
    {

        if (!amIDead) {
            Vector2 lineCastPos = myTrans.position - myTrans.up * myHeight * .5f;

            RaycastHit2D groundcast = Physics2D.Linecast(lineCastPos, lineCastPos + Vector2.down * .3f, enemyMask);
            bool isGrounded = groundcast;
            Debug.DrawLine(lineCastPos, lineCastPos + Vector2.down * .3f);

            lineCastPos = myTrans.position - myTrans.right * (myWidth / 2) - myTrans.up * myHeight * .5f;

            groundcast = Physics2D.Linecast(lineCastPos, lineCastPos + Vector2.down * 2, enemyMask);
            bool canGo = groundcast;
            Debug.DrawLine(lineCastPos, lineCastPos + Vector2.down * 2);

            RaycastHit2D wallcast = Physics2D.Linecast(lineCastPos, lineCastPos + new Vector2(-myTrans.right.x, myTrans.right.y) * .5f, enemyMask);
            bool isBlocked = wallcast;
            Debug.DrawLine(lineCastPos, lineCastPos + new Vector2(-myTrans.right.x, myTrans.right.y) * .5f);

            Quaternion nullrotation = Quaternion.identity;

            if (Gamemanager.gamemanager.characterPos < transform.position.x)
            {
                myTrans.eulerAngles = nullrotation.eulerAngles;
            }
            else
            {
                Vector3 rotate = nullrotation.eulerAngles;
                rotate.y += 180 * 1;
                myTrans.eulerAngles = rotate;
            }

            Quaternion directedrotation = myTrans.rotation;

            Vector2 myVel = myBody.velocity;

            if ((isBlocked || !canGo) && isGrounded)
            {
                myVel.y += jumpforce * Time.deltaTime;
                animator.SetBool("idle", true);
            }

            if (Mathf.Abs(Gamemanager.gamemanager.characterPos - transform.position.x) < 10 && canGo)
            {
                myVel.x = -myTrans.right.x * speed;
                animator.SetBool("idle", false);

            } else if (!isGrounded)
            {
                myVel.x = -myTrans.right.x * speed * 4;
            }
            else
            {
                myVel.x = 0;
                animator.SetBool("idle", true);
            }
            myBody.velocity = myVel;
        }else
        {

            myBody.rotation = 0;
            myBody.simulated = false;

        }
    }
}
