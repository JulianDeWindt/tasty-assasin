﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Corn : MonoBehaviour
{
    public int index;
    Image img;

    // Start is called before the first frame update
    void Start() {
        img = this.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update() {
        if (Gamemanager.gamemanager.ammo < index) {
            img.enabled = false;
        } else {
            img.enabled = true;
        }
    }
}
