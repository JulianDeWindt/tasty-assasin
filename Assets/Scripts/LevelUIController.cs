﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUIController : MonoBehaviour
{
    public Text scoreText;
    public GameObject background1;
    public GameObject background2;
    public GameObject background3;
    

    void Start()
    {
        Gamemanager.gamemanager.ammo = 3;
        Gamemanager.gamemanager.collectedBerries = 0;
        Gamemanager.gamemanager.killedEnemies = 0;
        Gamemanager.gamemanager.killedBerries = 0;
        Gamemanager.gamemanager.lives = 3;
    }

    // Update is called once per frame
    void Update()
    {
        Gamemanager.gamemanager.score = (int)Gamemanager.gamemanager.characterPos + Gamemanager.gamemanager.killedEnemies * 20 + Gamemanager.gamemanager.collectedBerries * 100 + Gamemanager.gamemanager.killedBerries * -50;
        scoreText.text = Gamemanager.gamemanager.score.ToString();

        int cPos = (int) Gamemanager.gamemanager.characterPos;
        int bgPos = (int) (cPos - cPos % 30);
        background1.transform.position = new Vector3(bgPos - 30, 6.5f, 0);
        background2.transform.position = new Vector3(bgPos, 6.5f, 0);
        background3.transform.position = new Vector3(bgPos + 30, 6.5f, 0);
    }
}