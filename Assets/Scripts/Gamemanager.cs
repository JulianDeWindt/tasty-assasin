﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gamemanager : MonoBehaviour {

    public static Gamemanager gamemanager = null;


    public int lives = 3;
    public float characterPos = 0.0f;
    public int score = 0;
    public int highscore = 0;
    public int killedEnemies = 0;
    public int killedBerries = 0;
    public int collectedBerries = 0;
    public int ammo = 3;
    public bool repeat = true;
    public float playerHurt = 0f;
    public int lastDistance = 0;
    public int bestDistance;

    // Start is called before the first frame update
    void Awake()
    {
        if(gamemanager == null)
        {
            gamemanager = this;
        } else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update() {
        if (playerHurt > 0.0f)
        {
            playerHurt -= Time.deltaTime;
            if (playerHurt < 0.0f)
            {
                playerHurt = 0.0f;
            }
        }
        if(lives <= 0)
        {
            Die();
        }
    }

    public void Die() {
        

        if (score > highscore) {
            highscore = score;
            bestDistance = (int)characterPos;
            PlayerPrefs.SetInt("highscore", highscore);
            PlayerPrefs.SetInt("bestDistance", bestDistance);
            PlayerPrefs.Save();
            
        }
        SceneManager.LoadScene("Startmenu");
        
        lastDistance = (int)characterPos;
    }    
}
